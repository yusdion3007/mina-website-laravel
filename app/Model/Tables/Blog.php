<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class Blog extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'blog';

    protected $fillable = [
        'id',
        'title',
        'hoc',
        'description',
        'image',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }
}
