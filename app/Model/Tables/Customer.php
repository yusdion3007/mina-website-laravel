<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class Customer extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'customer';

    protected $fillable = [
        'id',
        'name',
        'email',
        'phone_number',
        'package_id',
        'category',
        'instantion',
        'city_id',
        'city_name',
        'payment_method',
        'source',
        'referal_code',
        'source_other_ig',
        'to_bank_account',
        'from_bank_account',
        'amount_paid',
        'from_account_number',
        'from_account_name',
        'notes',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }

    public function package()
    {
        return $this->belongsTo('App\Model\Tables\TrainingPackage', 'package_id', 'id');
    }
}
