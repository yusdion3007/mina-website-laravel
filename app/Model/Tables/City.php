<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class City extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'city';

    protected $fillable = [
        'id',
        'name',
        'kab_code',
        'province_id',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }

    public function parent()
    {
        return $this->hasMany('App\Model\Tables\Comment', 'parent_id', 'id');
    }
}
