<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class About extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'about';

    protected $fillable = [
        'id',
        'header',
        'description',
        'address',
        'ext',
        'email',
        'web',
        'facebook',
        'instagram',
        'twitter',
        'start_hours',
        'end_hours',
        'image',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }
}
