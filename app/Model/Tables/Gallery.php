<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class Gallery extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'gallery';

    protected $fillable = [
        'id',
        'image',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }
}
