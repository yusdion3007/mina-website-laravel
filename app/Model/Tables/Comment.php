<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class Comment extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'comment';

    protected $fillable = [
        'id',
        'blog_id',
        'name',
        'email',
        'comment',
        'parent_id',
        'is_read',
        'read_by',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }

    public function parent()
    {
        return $this->hasMany('App\Model\Tables\Comment', 'parent_id', 'id');
    }
}
