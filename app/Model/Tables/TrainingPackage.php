<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class TrainingPackage extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'training_package';

    protected $fillable = [
        'id',
        'package_name',
        'price',
        'description',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }
}
