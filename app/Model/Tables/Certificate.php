<?php

namespace App\Model\Tables;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserGroup
 *
 * @author NawaTech
 */
class Certificate extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $table = 'certificate';

    protected $fillable = [
        'id',
        'name',
        'training_date',
        'training_type',
        'certificate_number',
        'exclusive_number',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    // public function template()
    // {
    //     return $this->belongsTo('App\Models\Tables\Template', 'template_id', 'id');
    // }
}
