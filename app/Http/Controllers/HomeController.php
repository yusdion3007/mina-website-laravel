<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\RegisterEmail;
use Illuminate\Support\Facades\Mail;

use App\Model\Tables\LandingPage;
use App\Model\Tables\About;
use App\Model\Tables\TrainingPackage;
use App\Model\Tables\Gallery;
use App\Model\Tables\Qoutes;
use App\Model\Tables\Blog;
use App\Model\Tables\Comment;
use App\Model\Tables\City;
use App\Model\Tables\Customer;
use App\Model\Tables\Certificate;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles All of Content in Site
    |
    */

    public function main(){
        try{
            $data = array();
            $landing_page = $this->loop(LandingPage::where('status', 1)->get());
            $about = About::first();
            $training_package = $this->loop(TrainingPackage::where('status', 1)->get());
            $gallery = $this->loop(Gallery::where('status', 1)->get());
            $qoutes = $this->loop(Qoutes::where('status', 1)->get());
            $blog = $this->loop(Blog::where('status', 1)->orderBy('id', 'desc')->skip(0)->take(3)->get(), "blog");

            $data['landing_page'] = $landing_page;
            $data['about'] = $about;
            $data['training_package'] = $training_package;
            $data['gallery'] = $gallery;
            $data['qoutes'] = $qoutes;
            $data['blog'] = $blog;

            return view('index')->with('data', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
        
    }

    protected function loop($data, $stage = ""){
        $data_seq = 1;
        foreach($data as $page){
            $page->create_date = $page->created_at->format('Y-m-d H:m:i');
            $page->no = $data_seq;

            if($stage == "blog"){
                $page->comment_total = Comment::where('blog_id', $page->id)->count();
            }

            $data_seq++;
        }
        return $data;
    }

    public function mainBlog(){
        try{
            $data = array();
            $landing_page = $this->loop(LandingPage::where('status', 1)->get());
            $about = About::first();
            $blog = $this->loop(Blog::where('status', 1)->orderBy('id', 'desc')->get(), "blog");
            $recent_post = $this->loop(Blog::where('status', 1)->orderBy('id', 'desc')->skip(0)->take(4)->get());

            $data['landing_page'] = $landing_page;
            $data['about'] = $about;
            $data['blog'] = $blog;
            $data['recent_post'] = $recent_post;

            return view('pages/blog')->with('data', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
    }

    public function blogPage($id){
        try{
            $data = array();
            $landing_page = $this->loop(LandingPage::where('status', 1)->get());
            $about = About::first();
            $blog = Blog::where('id', $id)->first();
            $recent_post = $this->loop(Blog::where('status', 1)->orderBy('id', 'desc')->skip(0)->take(4)->get());
            $comment = $this->loop(Comment::with(['parent'])->where('blog_id', $id)->where('status', 1)->whereNull('parent_id')->get());
            $comment_total = Comment::where('blog_id', $blog->id)->count();

            $blog->create_date = $blog->created_at->format('Y-m-d H:m:i');

            $data['landing_page'] = $landing_page;
            $data['about'] = $about;
            $data['blog'] = $blog;
            $data['recent_post'] = $recent_post;
            $data['comment'] = $comment;
            $data['comment_total'] = $comment_total;

            return view('pages/blogdetail')->with('data', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
    }

    public function postComment(Request $request){
        try{
            $comment = new Comment([
                'blog_id' => $request->get('blog_id'),
                'name' => $request->get('username'),
                'email' => $request->get('email'),
                'comment' => $request->get('comment'),
                'created_by' => $request->get('email')
            ]);
            $comment->save();

            return redirect('blog-page/'.$request->get('blog_id'));
        } catch (Exception $e) {
            report($e);

            return redirect('/');
        }
    }

    public function registerForm($id){
        try{
            $data = array();
            $training_package = TrainingPackage::where('id', $id)->first();
            $city = City::orderBy('name')->get();

            $data['training_package'] = $training_package;
            $data['city'] = $city;

            return view('pages/form')->with('data', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
    }

    public function registerTraining(Request $request){
        try{
            $customer = new Customer([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'phone_number' => $request->get('phone_number'),
                'package_id' => $request->get('package_id'),
                'category' => $request->get('category'),
                'instantion' => $request->get('instantion'),
                'city_id' => $request->get('city_id'),
                'city_name' => $request->get('city_name'),
                'payment_method' => $request->get('payment_method'),
                'source' => $request->get('source'),
                'referal_code' => $request->get('referal_code'),
                'source_other_ig' => $request->get('source_other_ig'),
                'to_bank_account' => $request->get('to_bank_account'),
                'from_bank_account' => $request->get('from_bank_account'),
                'amount_paid' => $request->get('amount_paid'),
                'from_account_number' => $request->get('from_account_number'),
                'from_account_name' => $request->get('from_account_name'),
                'notes' => $request->get('notes'),
                'status' => 2,
                'created_by' => $request->get('email')
            ]);
            $customer->save();
            $data = Customer::with(['package'])->where('id', $customer->id)->first();

            Mail::to($customer->email)->send(new RegisterEmail($data));

            return redirect('/');
        } catch (Exception $e) {
            report($e);

            return redirect('blog-page');
        }
    }

    public function invoice($id){
        try{
            $data = Customer::with(['package'])->where('id', $id)->first();
            return view('mails.demo')->with('demo', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
        
    }

    public function certificate(){
        try{
            $certificate = Certificate::get();
            $data = array(
                'certificate' => $certificate
            );
            return view('pages.sertifikat')->with('data', $data);
        } catch (Exception $e) {
            report($e);

            return $this->appResponse(2000);
        }
        
    }

}
