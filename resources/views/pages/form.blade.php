<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Mina Indonesia</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="{{ asset('img/favicon.png') }}" rel="icon">
    <link href="{{ asset('img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ asset('lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/venobox/venobox.css') }}" rel="stylesheet">

    <!-- Nivo Slider Theme -->
    <link href="{{ asset('css/nivo-slider-theme.css') }}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Responsive Stylesheet File -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar-example">
    <!-- MultiStep Form -->
    <div style="width: 100%">
        <div class="row">
            <div class="col-md-12">
                <form method="post" id="register" action="{{url('register-training')}}" enctype="multipart/form-data" class="msform">
                    {{csrf_field()}}
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active">Personal Dan Pelatihan</li>
                        <li>Kota Dan Sosial</li>
                        <li>Nama Pemilik Rekening</li>
                        <li>Berhasil</li>
                    </ul>
                    <!-- fieldsets -->
                    <fieldset>
                        <h2 class="fs-title">Personal Dan Pelatihan</h2>
                        <h3 class="fs-subtitle">Ayo Pilih Pelatihan Mu</h3>
                        <input type="text" name="name" placeholder="Nama Lengkap" />
                        <input type="email" name="email" placeholder="Email" />
                        <input type="number" id="phone_number" name="phone_number" placeholder="Nomor mu" required />
                        <select id="category" name="category" required>
                            <option value="" disabled selected>Kategori</option>
                            <option value="Siswa / Mahasiswa Aktif (D3/S1/S2)">Siswa / Mahasiswa Aktif (D3/S1/S2)</option>
                            <option value="Freshgraduate D3/S1">Freshgraduate D3/S1 (Maksimal 1 Tahun, dibuktikan dengan SKL atau Fotocopy Ijazah)</option>
                            <option value="Umum">Umum</option>
                        </select>
                        <input type="text" id="instantion" name="instantion" placeholder="Instansi" required>
                        <select id="package_id" name="package_id" aria-placeholder="Jenis Pelatihan" disabled>
                            <option value="" disabled selected>Pilih Pelatihan</option>
                            <option value="{{ $data['training_package']['id'] }}" selected>{{ $data['training_package']['package_name'] }} ( {{ $data['training_package']['description'] }} )</option>
                            <!-- <option>Paket 1 FSMS (ISO 22000, FSSC 22000, HACCP, GMP, SSOP)</option>
                                                <option>Paket 2 QHSE (ISO 9001, ISO 14001, ISO 45001, SMK 3 PP No 50 Th 2012)</option>
                                                <option>Keduanya (FSMS & QHSE)</option> -->
                        </select>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="notes" placeholder="Keterangan" rows="4"></textarea>
                        <input type="button" name="next" style="display:block" class="next action-button form-1-block" value="Next" disabled />
                        <input type="button" name="next" style="display:none" class="next action-button form-1" value="Next" />
                    </fieldset>

                    <fieldset>
                        <h2 class="fs-title">Kota Dan Sosial</h2>
                        <h3 class="fs-subtitle">Kasih tau dong tempat pelatihan mu</h3>
                        <select id="city_id" name="city_id" required>
                            @foreach($data['city'] as $list_city)
                            <option value="{{ $list_city->id }}">{{ $list_city->name }}</option>
                            @endforeach
                        </select>
                        <input type="text" id="exampleInputPassword1" name="city_name" placeholder="Jika Tidak ada, silahkan tulis nama kotamu disini (Contoh : Padang)">
                        <select id="inputState" id="payment_method" name="payment_method" required>
                            <option selected>Pembayaran Awal</option>
                            <option value="1">Lunas</option>
                            <option value="2">DP (minimal 300.000)</option>
                        </select>
                        <select id="inputState" id="source" name="source" required>
                            <option selected>Tahu Mina Dari mana?</option>
                            <option value="Email">Email</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Whatsapp">Whatsapp</option>
                            <option value="Line">Line</option>
                            <option value="Linkedin">Linkedin</option>
                            <option value="Telegram">Telegram</option>
                            <option value="Instagram Mina Indonesia">Instagram Mina Indonesia</option>
                            <option value="Instagram Akun Lain">Instagram Akun Lain</option>
                            <option value="Others">Others..</option>
                        </select>
                        <div style="text-align: left;">
                            <label>Jika kamu tahu dari Instagram Akun Lain, mohon sebutkan username akun infonya ya :)</label>
                        </div>
                        <input type="text" id="exampleInputPassword1" name="source_other_ig" placeholder="Akun Lainnya">
                        <input type="text" id="exampleInputPassword1" name="referal_code" placeholder="Referral (Di isi Jika Ada)">
                        <div style="text-align:center">
                            <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> -->
                            <input type="button" name="next" style="display:block" class="next action-button form-2-block" value="Next" disabled />
                            <input type="button" name="next" style="display:none" class="next action-button form-2" value="Next" />
                    </fieldset>

                    <fieldset>
                        <h2 class="fs-title">Detail Pembayaran</h2>
                        <h3 class="fs-subtitle">Isi dan Konfirmasi</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="Bank_Akun" name="to_bank_account" required>
                                        <option selected value="BCA">BCA</option>
                                        <option value="BRI">BRI</option>
                                        <option value="BNI">BNI Syariah</option>
                                    </select>
                                </div>
                                <div class="form-group d-none Akun_Bank" id="BNI">
                                    <ol>
                                        <li>Pilih <strong>Menu Lain</strong> &gt; <strong>Transfer</strong></li>
                                        <li>Pilih rekening asal dan pilih rekening tujuan ke rekening BNI Syariah</li>
                                        <li>Masukkan nomor rekening dengan nomor <b>808-000-220</b> dan pilih <strong>Benar</strong></li>
                                        <li>Masukkan jumlah pembayaran sejumlah tagihan Anda dan pilih <strong>Benar</strong></li>
                                        <li>Periksa data di layar. Pastikan Nama adalah nama penerima Anda di <strong>PT. Mina Maritim Indonesia</strong> dan <strong>Total Tagihan</strong> benar. Apabila data sudah benar, pilih <strong>Ya</strong></li>
                                    </ol>
                                </div>
                                <div class="form-group d-none Akun_Bank" id="BRI">
                                    <ol>
                                        <li>Pilih <strong>Menu Lain</strong> &gt; <strong>Transfer</strong></li>
                                        <li>Pilih rekening asal dan pilih rekening tujuan ke rekening BRI</li>
                                        <li>Masukkan nomor rekening dengan nomor <b>0259-0100-0334-300</b> dan pilih <strong>Benar</strong></li>
                                        <li>Masukkan jumlah pembayaran sejumlah tagihan Anda dan pilih <strong>Benar</strong></li>
                                        <li>Periksa data di layar. Pastikan Nama adalah nama penerima Anda di <strong>PT. Mina Maritim Indonesia</strong> dan <strong>Total Tagihan</strong> benar. Apabila data sudah benar, pilih <strong>Ya</strong></li>
                                    </ol>
                                </div>
                                <div class="form-group d-none Akun_Bank" id="BCA">
                                    <ol>
                                        <li>Pilih <strong>Menu Lain</strong> &gt; <strong>Transfer</strong></li>
                                        <li>Pilih rekening asal dan pilih rekening tujuan ke rekening BCA</li>
                                        <li>Masukkan nomor rekening dengan nomor <b>502-032-4122</b> dan pilih <strong>Benar</strong></li>
                                        <li>Masukkan jumlah pembayaran sejumlah tagihan Anda dan pilih <strong>Benar</strong></li>
                                        <li>Periksa data di layar. Pastikan Nama adalah nama penerima Anda di <strong>PT. Mina Maritim Indonesia</strong> dan <strong>Total Tagihan</strong> benar. Apabila data sudah benar, pilih <strong>Ya</strong></li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="inputState" name="from_bank_account" required>
                                        <option disabled selected>Dikirim Dari Bank</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BRI">BRI</option>
                                        <option value="BNI">BNI</option>
                                        <option value="DANAMON">DANAMON</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="number" id="exampleInputPassword1" name="amount_paid" placeholder="Nominal" required>
                                    <span>Training Price : Rp. {{ number_format($data['training_package']['price']) }}</span>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="exampleInputPassword1" name="from_account_number" placeholder="Dikirim Dari No. Rekening" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="exampleInputPassword1" name="from_account_name" placeholder="Nama Pemilik Rekening" required>
                                </div>
                            </div>
                        </div>
                        <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> -->
                        <input type="submit" name="submit" class="submit next action-button" value="Submit" />
                    </fieldset>

                    <fieldset>
                        <h2 class="fs-title">Selamat Pembayaran Berhasil</h2>
                        <h3 class="fs-subtitle">Silahkan Cek Email Anda</h3>
                        <div class="dummy-positioning">
                            <div class="success-icon">
                                <div class="success-icon__tip"></div>
                                <div class="success-icon__long"></div>
                            </div>
                            <h2 class="fs-title">Selamat!</h2>
                        </div>
                    </fieldset>
                </form>
                <!-- link to designify.me code snippets -->
                <div class="dme_link">
                    <p><a href="https://wa.me/6282285286729" target="_blank">Hubungi Kami</a></p>
                </div>
                <!-- /.link to designify.me code snippets -->
            </div>
        </div>
        <!-- /.MultiStep Form -->

        <div class="clearfix"></div>
    </div>

    <!-- JavaScript Libraries -->
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('lib/venobox/venobox.min.js') }}"></script>
    <script src="{{ asset('lib/knob/jquery.knob.js') }}"></script>
    <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('lib/parallax/parallax.js') }}"></script>
    <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('lib/appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

    <!-- Contact Form JavaScript File -->
    <script src="{{ asset('contactform/contactform.js') }}"></script>

    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    <script src="{{ asset('js/chosen.jquery.js') }}" type="text/javascript"></script>
    <script>
        $("#js-example-tags").select2({
            tags: true
        });
    </script>

    <script>
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function() {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'
                    });
                    next_fs.css({
                        'left': left,
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function() {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function() {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'left': left
                    });
                    previous_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'opacity': opacity
                    });
                },
                duration: 800,
                complete: function() {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".submit").click(function() {
            return false;
        })
    </script>
    <script>
        $(document).ready(function() {
            $("#name").change(function() {
                checkForm1();
            });
            $("#email").change(function() {
                checkForm1();
            });
            $("#phone_number").change(function() {
                checkForm1();
            });
            $("#instantion").change(function() {
                checkForm1();
            });
            $("#category").change(function() {
                checkForm1();
            });

            function checkForm1() {
                var name = $("#name").val();
                var email = $("#email").val();
                var phone_number = $("#phone_number").val();
                var instantion = $("#instantion").val();
                var category = $("#category").val();
                if (name != "" && email != "" && instantion != "" && phone_number != "" && category != "") {
                    $('.form-1').css('display', 'block');
                    $('.form-1-block').css('display', 'none');
                } else {
                    $('.form-1').css('display', 'none');
                    $('.form-1-block').css('display', 'block');
                }
            }

            $("#city_id").change(function() {
                checkForm2();
            });
            $("#payment_method").change(function() {
                checkForm2();
            });
            $("#source").change(function() {
                checkForm2();
            });

            function checkForm2() {
                var city_id = $("#city_id").val();
                var payment_method = $("#payment_method").val();
                var source = $("#source").val();
                if (city_id != "" && payment_method != "" && source != "") {
                    $('.form-2').css('display', 'block');
                    $('.form-2-block').css('display', 'none');
                } else {
                    $('.form-2').css('display', 'none');
                    $('.form-2-block').css('display', 'block');
                }
            }

            $('#Bank_Akun').on('change', function() {
                $('.Akun_Bank').removeClass('d-block');
                $('#' + this.value).addClass('d-block');
            });

        });
    </script>
</body>

</html>