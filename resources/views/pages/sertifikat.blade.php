<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Mina Indonesia</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="datatable/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet">
  <!-- Libraries CSS Files -->
  <link href="lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="css/responsive.css" rel="stylesheet">

  <style>
    .paddingNufi {
      padding: 180px 60px;;
    }
  </style>

</head>

<body data-spy="scroll" data-target="#navbar-example">

  <div id="preloader"></div>

  <!-- top nav -->

  <header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">

            <!-- Navigation -->
            <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
                <!-- Brand -->
                <a class="navbar-brand page-scroll sticky-logo" href="{{url('')}}">
                  <img src="{{ asset('img/logo.png') }}" alt="logo-mina-indonesia" width="100">
				        </a>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                <ul class="nav navbar-nav navbar-right">
                  <li>
                    <a class="page-scroll" href="{{url('')}}">Home</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="{{url('')}}">About</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="{{url('')}}">Paket</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="{{url('')}}">Gallery</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="{{url('')}}">Blog</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="{{url('')}}">Contact</a>
                  </li>
                  <li class="active">
                    <a class="page-scroll" href="{{url('sertifikat')}}">Sertifikat</a>
                  </li>
                </ul>
              </div>
              <!-- navbar-collapse -->
            </nav>
            <!-- END: Navigation -->
          </div>
        </div>
      </div>
    </div>
    <!-- header-area end -->
  </header>
  <!-- header end -->

  <!-- Start Bottom Header -->
  <div class="header-bg page-area" style="height: 60%">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="paddingNufi text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">List Sertifikat</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="blog-page area-padding">
    <div class="container">
      <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
        <thead>
          <tr>
            <th>Name</th>
            <th>Jenis Pelatihan</th>
            <th>Tanggal</th>
            <th>No. Sertifikat</th>
            <th>No. Ekslusif</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data['certificate'] as $certificate)
          <tr>
            <td>{{ $certificate->name }}</td>
            <td>{{ $certificate->training_type }}</td>
            <td>{{ $certificate->training_date }}</td>
            <td>{{ $certificate->certificate_number }}</td>
            <td>{{ $certificate->exclusive_number }}</td>
          </tr>
          @endforeach
          <!-- <tr>
            <td>Andri</td>
            <td>System Architect</td>
            <td>Bandung, QHSE 20 Oktober 2019</td>
            <td>123984729387</td>
            <td>12-12-2012</td>
          </tr>
          <tr>
            <td>Sitong</td>
            <td>System Architect</td>
            <td>Bandung, QHSE 20 Oktober 2019</td>
            <td>123984729387</td>
            <td>12-12-2012</td>
          </tr>
          <tr>
            <td>Tiger Waksi</td>
            <td>System Architect</td>
            <td>Bandung, QHSE 20 Oktober 2019</td>
            <td>123984729387</td>
            <td>12-12-2012</td>
          </tr> -->
        </tbody>
        <tfoot>
          <tr>
            <th>Name</th>
            <th>Jenis Pelatihan</th>
            <th>Tanggal</th>
            <th>No. Sertifikat</th>
            <th>No. Ekslusif</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="clearfix"></div>

  <!-- bot -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/venobox/venobox.min.js"></script>
  <script src="lib/knob/jquery.knob.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/parallax/parallax.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="lib/appear/jquery.appear.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="datatable/js/jquery.dataTables.min.js"></script>
  <script src="datatable/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
  <script>
    $(document).ready(function() {
      var table = $('#example').DataTable({
        responsive: true
      });

      new $.fn.dataTable.FixedHeader(table);
    });
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <script src="js/main.js"></script>
</body>

</html>