<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@main');
Route::get('blog', 'HomeController@mainBlog');
Route::get('blog-page/{id}', 'HomeController@blogPage');
Route::get('register-form/{id}', 'HomeController@registerForm');
Route::post('register-training', 'HomeController@registerTraining');
Route::get('invoice/{id}', 'HomeController@invoice');

Route::get('form-daftar', function () {
    return view('pages/form');
});
Route::get('sertifikat', 'HomeController@certificate');
Route::get('sertifikat-backup', function () {
    return view('pages/sertifikat');
});
Route::post('post-comment', 'HomeController@postComment');

// Route::get('/blog', function () {
//     return view('pages/blog');
// });

// Route::get('/blog-page', function () {
//     return view('pages/blogdetail');
// });
